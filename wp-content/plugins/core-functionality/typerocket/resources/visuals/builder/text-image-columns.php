<?php $context   = Timber::get_context();
$context['data'] = $data;
$templates       = array( 'text-image-columns.twig' );
Timber::render( $templates, $context );