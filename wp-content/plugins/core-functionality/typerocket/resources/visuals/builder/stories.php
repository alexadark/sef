<?php
$context         = Timber::get_context();
$context['data'] = $data;
$templates       = array( 'stories.twig' );
Timber::render( $templates, $context );