<?php $context   = Timber::get_context();
$context['data'] = $data;
$templates       = array( 'photo-text.twig' );
Timber::render( $templates, $context );