<?php 
$context   = Timber::get_context();
$context['data'] = $data;
$templates = array( 'links.twig' );
Timber::render( $templates, $context );