<?php 
$context   = Timber::get_context();
$context['data'] = $data;
$templates = array( 'numbers.twig' );
Timber::render( $templates, $context );