<?php
$context   = Timber::get_context();
$context['data'] = $data;
$templates = array( 'logos.twig' );
Timber::render( $templates, $context );