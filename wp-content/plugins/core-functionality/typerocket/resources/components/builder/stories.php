<h1>Stories</h1>
<p>Choose the stories you want to display here</p>
<?php
echo $form->checkbox('Grey Background');
echo $form
	->select( 'Number of columns' )
	->setOptions( array(
		'2 columns' => 'uk-child-width-1-2@m',
		'3 Columns' => 'uk-child-width-1-3@m',
		'4 Columns' => 'uk-child-width-1-4@m'
	) );
echo $form->repeater('Client Stories')->setFields(array(
$form->search('Add Story')->setPostType('stories'),
	$form->image('Image')
));



