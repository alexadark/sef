<h1>Photo Text Columns</h1>
<p>You can choose the number of columns 2, 3 or 4</p>
<?php
echo $form->checkbox('Grey Background');
echo $form
	->select( 'Number of columns' )
	->setOptions( array(
		'2 columns' => 'uk-child-width-1-2@m',
		'3 Columns' => 'uk-child-width-1-3@m',
		'4 Columns' => 'uk-child-width-1-4@m'
	) );

echo $form->repeater( 'Text Image' )->setFields( array(
	$form->editor( 'Text' ),
	$form->image( 'Image' )
) );
