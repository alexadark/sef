<h1>Photo Text Component</h1>
<p>You can have several rows, then the order will be inversed on each row</p>
<?php
echo $form->checkbox( 'Grey Background' );
echo $form->repeater( 'Photo Text' )->setFields( array(
	$form->image( 'Photo' ),
	$form->editor( 'Text' ),
) );
