<?php

//Headings on Top Image

$headings = tr_meta_box('Top Headings');
$headings-> addScreen(array('page','stories', 'post'));
$headings->setPriority('high');
$headings->setCallback(function(){
	$form = tr_form();
	echo $form->editor('Header Text');
});


//Sub footer at the bottom of every page

$footer = tr_meta_box('Sub-footer Links');
$footer-> addScreen(array('stories','page', 'post'));
$footer->setCallback(function(){
	$form = tr_form();
	echo $form->textarea('Subfooter Text');
	echo $form->repeater('Subfooter Links')->setFields(array(
		$form->text('Title'),
		$form->text('Url'),
		$form-> checkbox('External Link')->setText('Yes')
	));
});




//Builder stories
$builder = tr_meta_box('Story builder');
$builder->addScreen(array('stories'));
$builder->setCallback(function(){
$form = tr_form();
 echo $form->builder('Modules');
});