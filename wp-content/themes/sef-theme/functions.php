<?php
/**
 * SEF.
 *
 * This file adds functions to the Genesis SEF Theme.
 *
 * @package SEF * @author  Alexandra Spalato
 * @license GPL-2.0+
 * @link    http://alexandraspalato.com/
 */

// Start the engine.
include_once( get_template_directory() . '/lib/init.php' );

//Initialize theme constants

$child_theme = wp_get_theme();

define( 'CHILD_THEME_NAME', $child_theme->get( 'Name' ) );
define( 'CHILD_THEME_URL', $child_theme->get( 'ThemeURI' ) );
define( 'CHILD_THEME_VERSION', $child_theme->get( 'Version' ) );
define( 'CHILD_TEXT_DOMAIN', $child_theme->get( 'TextDomain' ) );

define( 'CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'CHILD_URI', get_stylesheet_directory_uri() );
define( 'CHILD_CONFIG_DIR', CHILD_THEME_DIR . '/config/' );
define( 'CHILD_LIB', CHILD_THEME_DIR . '/lib/' );
define( 'CHILD_IMG', CHILD_URI . '/assets/images/' );
define( 'CHILD_JS', CHILD_URI . '/assets/js/' );

// Setup Theme.
include_once( CHILD_THEME_DIR . '/lib/theme-defaults.php' );

// Set Localization (do not remove).
add_action( 'after_setup_theme', 'wst_localization_setup' );
function wst_localization_setup() {
	load_child_theme_textdomain( 'genesis-sample', CHILD_THEME_DIR . 'assets/languages' );
}

// Add the helper functions.
include_once( CHILD_THEME_DIR . '/lib/helper-functions.php' );


// Include Kirki custom controls.
require_once( CHILD_THEME_DIR . '/lib/customizer/include-kirki.php' );


// Include Kirki fallback.
require_once( CHILD_THEME_DIR . '/lib/customizer/kirki-fallback.php' );


//  WordPress Theme Customizer.
require_once( CHILD_THEME_DIR . '/lib/customizer/customizer.php' );

// Include Customizer CSS.
//include_once( CHILD_THEME_DIR . '/lib/customizer/customizer-css.php' );


/*-----------------------------------------------------------
	LOAD ASSETS
/*------------------------------------------------------------*/

// Enqueue Scripts and Styles.
add_action( 'wp_enqueue_scripts', 'wst_enqueue_scripts_styles' );
function wst_enqueue_scripts_styles() {

	wp_enqueue_style( 'genesis-sample-fonts', '//fonts.googleapis.com/css?family=Oswald:300,400|Raleway:400,400i,600,700', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'dashicons' );

	wp_enqueue_script( 'uikit-js', '//cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.28/js/uikit.min.js', array( 'jquery' ), '3.0.0-beta.28', true );

	wp_enqueue_script( 'uikit-icons-js', '//cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.28/js/uikit-icons.min.js',
		array( 'uikit-js' ),
		'3.0.0-beta.28', true );
	wp_enqueue_script( 'theme-js', CHILD_JS . 'theme.js', array(
		'jquery',
		'jquery.matchHeight.min.js'
	), CHILD_THEME_VERSION, true );

	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script( 'genesis-sample-responsive-menu', CHILD_JS . "responsive-menus{$suffix}.js", array( 'jquery' ), CHILD_THEME_VERSION, true );
	wp_localize_script(
		'genesis-sample-responsive-menu',
		'genesis_responsive_menu',
		wst_responsive_menu_settings()
	);

}

add_action( 'wp_enqueue_scripts', 'wst_enqueue_backstretch' );
function wst_enqueue_backstretch() {
	if (  ! has_post_thumbnail() || is_page( 'contact-us' ) ) {
		return;
	}

	wp_enqueue_script( 'backstretch', CHILD_URI . '/assets/js/jquery.backstretch-overlay.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'backstretch-set', CHILD_URI . '/assets/js/backstretch-set.js', array( 'backstretch' ), '1.0.0', true );

	$backstretch_src = array( 'src' => wp_get_attachment_url( get_post_thumbnail_id() ) );

	wp_localize_script( 'backstretch-set', 'BackStretchImg', $backstretch_src );


}

// Define our responsive menu settings.
function wst_responsive_menu_settings() {

	$settings = array(
		'mainMenu'          => __( 'Menu', 'genesis-sample' ),
		'menuIconClass'     => 'dashicons-before dashicons-menu',
		'subMenu'           => __( 'Submenu', 'genesis-sample' ),
		'subMenuIconsClass' => 'dashicons-before dashicons-arrow-down-alt2',
		'menuClasses'       => array(
			'combine' => array(
				'.nav-primary',
				'.nav-header',
			),
			'others'  => array(),
		),
	);

	return $settings;

}

// Change order of main stylesheet to override plugin styles.
remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
add_action( 'wp_enqueue_scripts', 'genesis_enqueue_main_stylesheet', 99 );

/*-----------------------------------------------------------
	THEME SUPPORTS
/*------------------------------------------------------------*/

// Add HTML5 markup structure.
add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );

// Add Accessibility support.
add_theme_support( 'genesis-accessibility', array(
	'404-page',
	'drop-down-menu',
	'headings',
	'rems',
	'search-form',
	'skip-links'
) );

// Add viewport meta tag for mobile browsers.
add_theme_support( 'genesis-responsive-viewport' );

// Add support for custom header.
add_theme_support( 'custom-header', array(
	'width'           => 111,
	'height'          => 50,
	'header-selector' => '.site-title a',
	'header-text'     => false,
	'flex-height'     => true,
) );

// Add support for custom background.
add_theme_support( 'custom-background' );

// Add support for after entry widget.
add_theme_support( 'genesis-after-entry-widget-area' );

// Add support for 3-column footer widgets.
//add_theme_support( 'genesis-footer-widgets', 3 );

//add support for structural wraps
add_theme_support( 'genesis-structural-wraps', array(
	'site-inner',
	'header',
	'nav',
	'subnav',
	'footer-widgets',
	'footer'
) );

/*-----------------------------------------------------------
	REMOVE SITE LAYOUTS
/*------------------------------------------------------------*/

genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );


/*-----------------------------------------------------------
	IMAGE SIZES
/*------------------------------------------------------------*/

add_image_size( 'featured-image', 840, 400, true );
/*-----------------------------------------------------------
	WIDGET AREAS
/*------------------------------------------------------------*/
genesis_register_widget_area(
	array(
		'id'          => 'footer',
		'name'        => __( 'Footer', CHILD_TEXT_DOMAIN ),
		'description' => __( 'This is the footer widgets section', CHILD_TEXT_DOMAIN ),
	)
);


/*-----------------------------------------------------------
	NAV
/*------------------------------------------------------------*/

// Rename primary and secondary navigation menus.
add_theme_support( 'genesis-menus', array(
	'primary'   => __( 'After Header Menu', 'genesis-sample' ),
	'secondary' => __( 'Footer Menu', 'genesis-sample' )
) );

// Reposition the secondary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 5 );

// Reduce the secondary navigation menu to one level depth.
//add_filter( 'wp_nav_menu_args', 'wst_secondary_menu_args' );
function wst_secondary_menu_args( $args ) {

	if ( 'secondary' != $args['theme_location'] ) {
		return $args;
	}

	$args['depth'] = 1;

	return $args;

}

/*-----------------------------------------------------------
	GRAVATAR
/*------------------------------------------------------------*/


// Modify size of the Gravatar in the author box.
add_filter( 'genesis_author_box_gravatar_size', 'wst_author_box_gravatar' );
function wst_author_box_gravatar( $size ) {
	return 90;
}

// Modify size of the Gravatar in the entry comments.
add_filter( 'genesis_comment_list_args', 'wst_comments_gravatar' );
function wst_comments_gravatar( $args ) {

	$args['avatar_size'] = 60;

	return $args;

}

/*-----------------------------------------------------------
	HEADER
/*------------------------------------------------------------*/

if ( ! wp_is_mobile() ) {
	add_filter( 'genesis_attr_site-header', 'wst_change_site_header_attr', 99 );
}
/**
 * Sticky menu on desktop
 *
 * @since 1.0.0
 *
 * @param string $attr sticky attribute
 *
 *
 */
function wst_change_site_header_attr( $attr ) {
	$attr['uk-sticky'] = "top: 300; animation: uk-animation-slide-top";

	return $attr;
}

//add_action( 'genesis_before_header', 'wst_open_header_wrap', 15 );
///**
// * Open header wrap around header slider and top image
// *
// * @since 1.0.0
// *
// */
//function wst_open_header_wrap() {
//	echo '<div class="header-wrap">';
//}
//
//add_action( 'genesis_after_header', 'wst_close_header_wrap', 15 );
///**
// * Open header wrap around header slider and top image
// *
// * @since 1.0.0
// *
// */
//function wst_close_header_wrap() {
//	echo '</div>';
//}

add_action( 'genesis_after_header', 'wst_display_header_image' );
function wst_display_header_image() {
	if (  is_page_template( 'contact.php' ) ) {
		return;
	}
	$context   = Timber::get_context();
	$templates = array( 'header-image.twig' );
	Timber::render( $templates, $context );

}

/*-----------------------------------------------------------
	POST
/*------------------------------------------------------------*/
//Reposition featured image in archive.

remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );

add_action( 'genesis_before_entry', 'genesis_do_post_image' );

add_action( 'genesis_before_entry', 'wst_show_featured_image_single_post' );

/**
 * Display featured image (if present) before entry on single Posts
 */

function wst_show_featured_image_single_post() {
	if ( ! ( is_singular( 'post' ) && has_post_thumbnail() ) ) {
		return;
	}

	$args = array(
		'size' => 'featured-image',
		'attr' => array(
			'class' => 'featured-image',
		),
	);
	genesis_image( $args );

}

//Remove archive title description
remove_action( 'genesis_before_loop', 'genesis_do_posts_page_heading' );

//remove page title on pages

add_action( 'wp', 'wst_remove_page_titles' );
/**
 * Remove page titles
 *
 * @since 1.0.0
 *
 * @return void
 */
function wst_remove_page_titles() {
	if ( ! is_singular( 'page' ) ) {
		return;
	}
	remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
}

//add page builder on default pages
add_action( 'wp', 'wst_add_builder_on_default_pages' );
function wst_add_builder_on_default_pages(){
    if(!is_singular('page')){
    	return;
    }
    add_action( 'genesis_entry_content', 'wst_display_builder' );
    function wst_display_builder(){
	    tr_components_field('builder');
    }
}


/*-----------------------------------------------------------
	FOOTER
/*------------------------------------------------------------*/
add_action( 'genesis_before_footer', 'wst_display_subfooter', 2 );
function wst_display_subfooter() {
	$context   = Timber::get_context();
	$templates = array( 'subfooter.twig' );
	Timber::render( $templates, $context );


}


//* Customize the entire footer
remove_action( 'genesis_footer', 'genesis_do_footer' );
add_action( 'genesis_footer', 'wst_custom_footer' );
function wst_custom_footer() {
	genesis_widget_area( 'footer', array(
		'before' => '<div id="footer" class="uk-text-center">',
		'after'  => '</div>'
	) );
}



